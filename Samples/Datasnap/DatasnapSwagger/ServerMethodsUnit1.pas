unit ServerMethodsUnit1;

interface

uses
  GBSwagger.Model.Interfaces,
  GBSwagger.Path.Attributes,
  Web.HTTPApp, Datasnap.DSHTTPWebBroker,
  System.SysUtils, System.Classes, System.Json,
  GBSwagger.Web.HTML,
  Datasnap.DSServer, Datasnap.DSAuth,
  Datasnap.Classes,
  GBSwagger.Path.Register;

type
{$METHODINFO ON}
  [SwagPath('Usuarios', 'usuario')]
  TServerMethods1 = class(TDataModule)
  public
    [SwagGET('Lista Usuarios')]
    [SwagResponse(200, TUser, 'Lista de Usuarios', True)]
    [SwagResponse(400, TAPIError)]
    function usuario: TJSONValue;

    [SwagPOST('Insere um usuario')]
    [SwagParamBody('usuario', TUser, '')]
    [SwagResponse(201, TUser, 'Created')]
    [SwagResponse(400, TAPIError, 'Bad Request')]
    [SwagResponse(500, TAPIError, 'Internal Server Error')]
    function updateUsuario: TJSONValue;

    [SwagPUT('Altera Usuario', '{id}')]
    [SwagParamPath('id', 'id do usuario')]
    [SwagParamBody('usuario', TUser, '')]
    [SwagResponse(204)]
    [SwagResponse(400, TAPIError, 'Bad Request')]
    [SwagResponse(500, TAPIError, 'Internal Server Error')]
    function acceptUsuario(IdUsuario: Integer): TJSONValue;

    [SwagDELETE('Excluir Usuario', '{id}')]
    [SwagParamPath('id', 'id do usuario')]
    [SwagResponse(204)]
    [SwagResponse(400, TAPIError, 'Bad Request')]
    [SwagResponse(500, TAPIError, 'Internal Server Error')]
    function cancelUsuario: TJSONValue;

  end;
{$METHODINFO OFF}

implementation

{$R *.dfm}

{ TServerMethods1 }

function TServerMethods1.acceptUsuario(IdUsuario: Integer): TJSONValue;
begin

end;

function TServerMethods1.cancelUsuario: TJSONValue;
begin

end;

function TServerMethods1.updateUsuario: TJSONValue;
begin

end;

function TServerMethods1.usuario: TJSONValue;
begin

end;

initialization
  Swagger
    .Info
      .Title('API Test')
    .&End
    .Host('localhost:8081')
    .BasePath('datasnap/rest');

  TGBSwaggerPathRegister.RegisterPath(TServerMethods1);
end.

