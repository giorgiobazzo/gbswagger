unit GBSwagger.Register;

interface

uses
  GBSwagger.Model.Interfaces,
  GBSwagger.Register.Interfaces,
  System.Generics.Collections;

type TGBSwaggerRegister = class(TInterfacedObject, IGBSwaggerRegister)

  protected
    [Weak]
    FParent: IGBSwagger;
    FResponse: IGBSwaggerRegisterResponse;
    FListResponses: TList<Integer>;

    function ResponseExists(StatusCode: Integer): Boolean;
    function Response(StatusCode: Integer): IGBSwaggerRegisterResponse;

    function &End: IGBSwagger;
  public
    constructor create(Parent: IGBSwagger);
    class function New(Parent: IGBSwagger): IGBSwaggerRegister;
    destructor Destroy; override;
end;

implementation

{ TGBSwaggerRegister }

uses
  GBSwagger.Register.Response;

destructor TGBSwaggerRegister.Destroy;
begin
  FListResponses.free;
  inherited;
end;

function TGBSwaggerRegister.&End: IGBSwagger;
begin
  result := FParent;
end;

constructor TGBSwaggerRegister.create(Parent: IGBSwagger);
begin
  FParent       := Parent;
  FResponse     := TGBSwaggerRegisterResponse.New(Self);
  FListResponses:= TList<Integer>.create;
end;

class function TGBSwaggerRegister.New(Parent: IGBSwagger): IGBSwaggerRegister;
begin
  Result := Self.create(Parent);
end;

function TGBSwaggerRegister.Response(StatusCode: Integer): IGBSwaggerRegisterResponse;
begin
  result := FResponse.Register(StatusCode);
  if not FListResponses.Contains(StatusCode) then
    FListResponses.Add(StatusCode);
end;

function TGBSwaggerRegister.ResponseExists(StatusCode: Integer): Boolean;
begin
  result := FListResponses.Contains(StatusCode);
end;

end.
