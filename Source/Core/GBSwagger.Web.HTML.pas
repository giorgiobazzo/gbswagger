unit GBSwagger.Web.HTML;

interface

uses
  System.SysUtils,
  System.Classes,
  System.StrUtils;

function SwaggerDocument(AResourcePath, AJsonPath: String): string; overload;

implementation

uses GBSwagger.Model.Interfaces;

function SwaggerDocument(AResourcePath, AJsonPath: String): string;
var
  html: TStringList;
begin
  html := TStringList.Create;
  try
    html.Add('<!DOCTYPE html>');
    html.Add('<html lang="en">');
    html.Add('  <head>');
    html.Add('    <meta charset="UTF-8">');
    html.Add('    <title>'+Swagger.Info.Title+'</title>');
    html.Add(format('    <link rel="stylesheet" type="text/css" href="%s/swagger-ui.css" >', [AResourcePath]));
    html.Add('    <style>');
    html.Add('      html');
    html.Add('      {');
    html.Add('        box-sizing: border-box;');
    html.Add('        overflow: -moz-scrollbars-vertical;');
    html.Add('        overflow-y: scroll;');
    html.Add('      }');
    html.Add('');
    html.Add('      *,');
    html.Add('      *:before,');
    html.Add('      *:after');
    html.Add('      {');
    html.Add('        box-sizing: inherit;');
    html.Add('      }');
    html.Add('');
    html.Add('      body');
    html.Add('      {');
    html.Add('        margin:0;');
    html.Add('        background: #fafafa;');
    html.Add('      }');
    html.Add('    </style>');
    html.Add('  </head>');
    html.Add('');
    html.Add('  <body>');
    html.Add('    <div id="swagger-ui"></div>');
    html.Add('                                  ');
    html.Add(format('    <script src="%s/swagger-ui-bundle.js"> </script>', [AResourcePath]));
    html.Add(format('    <script src="%s/swagger-ui-standalone-preset.js"> </script>', [AResourcePath]));
    html.Add('    <script>');
    html.Add('    window.onload = function() {');
    html.Add('      // Begin Swagger UI call region');
    html.Add('      const ui = SwaggerUIBundle({');
    html.Add(format('        url: "%s", ',[AJsonPath]));
    html.Add('        dom_id: ''#swagger-ui'',');
    html.Add('        deepLinking: true,');
    html.Add('        presets: [');
    html.Add('          SwaggerUIBundle.presets.apis,');
    html.Add('          SwaggerUIStandalonePreset');
    html.Add('        ],');
    html.Add('        plugins: [');
    html.Add('          SwaggerUIBundle.plugins.DownloadUrl');
    html.Add('        ],');
    html.Add('        layout: "StandaloneLayout"');
    html.Add('      })');
    html.Add('      // End Swagger UI call region');
    html.Add('');
    html.Add('      window.ui = ui');
    html.Add('    }');
    html.Add('  </script>');
    html.Add('  </body>');
    html.Add('</html>');

    result := html.Text;
  finally
    html.Free;
  end;
end;

end.
