unit GBSwagger.Path.Register;

interface

uses
  System.SysUtils,
  System.Generics.Collections;

type TGBSwaggerPathRegister = class

  strict private
    class var
      FInstance: TGBSwaggerPathRegister;
      FClassesPath: TList<TClass>;

  public
    class procedure RegisterPath(AClass: TClass);
    class function GetClasses: TArray<TClass>;

    class function Instance: TGBSwaggerPathRegister;
    class constructor create;
    class destructor  Destroy;
end;

implementation

{ TGBSwaggerPathRegister }

class constructor TGBSwaggerPathRegister.create;
begin
  FClassesPath := TList<TClass>.Create;
end;

class destructor TGBSwaggerPathRegister.Destroy;
begin
  FClassesPath.Free;
  if Assigned(FInstance) then
    FInstance.Free;
  inherited;
end;

class function TGBSwaggerPathRegister.GetClasses: TArray<TClass>;
begin
  result := FInstance.FClassesPath.ToArray;
end;

class function TGBSwaggerPathRegister.Instance: TGBSwaggerPathRegister;
begin
  if not Assigned(Self.FInstance) then
    FInstance := TGBSwaggerPathRegister.Create;
  result := FInstance;
end;

class procedure TGBSwaggerPathRegister.RegisterPath(AClass: TClass);
begin
  if not Instance.FClassesPath.Contains(AClass) then
    Instance.FClassesPath.Add(AClass);
end;

end.
