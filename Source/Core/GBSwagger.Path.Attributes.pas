unit GBSwagger.Path.Attributes;

interface

uses
  System.SysUtils;

type
  SwagApp = class(TCustomAttribute)
  private
    FTitle: string;
    FVersion: String;
    FHost: string;

  public
    property title: String read FTitle;
    property version: string read FVersion;
    property host: string read FHost;

    constructor create(ATitle, AVersion: string; AHost: String = ''); overload;
    constructor create(ATitle: String); overload;
  end;

  SwagBasePath = class(TCustomAttribute)
  private
    FValue: string;
  public
    constructor create(Value: String);
    property value: String read FValue;
  end;

  SwagAppDescription = class(TCustomAttribute)
  private
    FDescription: string;
  public
    constructor create(ADescription: String);
    property description: string read FDescription;
  end;

  SwagSecurityBearer = class(TCustomAttribute)
  end;

  SwagSecurityBasic = class(TCustomAttribute)
  end;

  SwagPath = class(TCustomAttribute)
  private
    Fname: string;
    Ftag: String;
  public
    property name: string read Fname;
    property tag: String read Ftag write Ftag;

    constructor create(ATag: String; AName: String = '');
  end;

  SwagParam = class(TCustomAttribute)
  private
    Fname: String;
    Fdescription: String;
    Frequired: Boolean;
    FisArray: Boolean;
    Fschema: String;
  public
    property name: String read Fname;
    property description: String read Fdescription;
    property required: Boolean read Frequired;
    property isArray: Boolean read FisArray;
    property schema: String read Fschema write Fschema;

    constructor create(AName: string; ADescription: string = ''); overload;
    constructor create(AName, ADescription, ASchema: String; bRequired: Boolean = True; bIsArray: Boolean = False); overload;
  end;

  SwagParamPath = class(SwagParam)
  end;

  SwagParamHeader = class(SwagParam)
  end;

  SwagParamQuery = class(SwagParam)
  end;

  SwagParamBody = class(SwagParam)
  private
    FclassType: TClass;
  public
    function classType: TClass; reintroduce;

    constructor create(AName: string; AClassType: TClass; ADescription: string = ''); reintroduce; overload;
    constructor create(AName: string; AClassType: TClass; bIsArray: Boolean = False; ADescription: string = ''; bRequired: Boolean = True); reintroduce; overload;
  end;

  SwagEndPoint = class(TCustomAttribute)
  private
    Fsummary: string;
    Fdescription: string;
    Fpath: string;
    Fpublic: Boolean;
  public
    property path: string read Fpath;
    property summary: string read Fsummary;
    property description: string read Fdescription;
    property isPublic: Boolean read Fpublic;

    constructor create(ASummary: String; APath: String = ''; ADescription: String = ''; APublic: Boolean = False); overload;
    constructor create(ASummary: String; APath: String; APublic: Boolean; ADescription: String = ''); overload;
  end;

  SwagGET = class(SwagEndPoint)
  end;

  SwagPOST = class(SwagEndPoint)
  end;

  SwagPUT = class(SwagEndPoint)
  end;

  SwagDELETE = class(SwagEndPoint)
  end;

  SwagResponse = class(TCustomAttribute)
  private
    FhttpCode: Integer;
    Fdescription: string;
    Fschema: string;
    FclassType: TClass;
    FisArray: Boolean;
  public
    property httpCode: Integer read FhttpCode;
    property description: string read Fdescription;
    property schema: string read Fschema;
    property isArray: Boolean read FisArray;

    function classType: TClass; reintroduce;

    constructor create(AHttpCode    : Integer;
                       AClassType   : TClass;
                       ADescription : String = '';
                       bIsArray     : Boolean = False); overload;

    constructor create(AHttpCode    : Integer;
                       ASchema      : String;
                       ADescription : String = '';
                       bIsArray     : Boolean = False); overload;

    constructor create(AHttpCode: Integer); overload;
  end;

implementation

{ SwagPath }

constructor SwagPath.create(ATag: String; AName: String = '');
begin
  Fname := AName;
  Ftag  := ATag;
end;

{ SwagEndPoint }

constructor SwagEndPoint.create(ASummary: String; APath: String = ''; ADescription: String = ''; APublic: Boolean = False);
begin
  Fsummary     := ASummary;
  Fdescription := ADescription;
  Fpath        := APath;
  Fpublic      := APublic;
end;

constructor SwagEndPoint.create(ASummary, APath: String; APublic: Boolean; ADescription: String);
begin
  create(ASummary, APath, ADescription, APublic);
end;

{ SwagParam }

constructor SwagParam.create(AName, ADescription: string);
begin
  Fname        := AName;
  Fdescription := ADescription;
  Fschema      := 'string';
  FisArray     := False;
  Frequired    := True;
end;

constructor SwagParam.create(AName, ADescription, ASchema: String; bRequired, bIsArray: Boolean);
begin
  create(AName, ADescription);
  Fschema   := ASchema;
  Frequired := bRequired;
  FisArray  := bIsArray;
end;

{ SwagParamBody }

function SwagParamBody.classType: TClass;
begin
  result := FclassType;
end;

constructor SwagParamBody.create(AName: string; AClassType: TClass; bIsArray: Boolean; ADescription: string; bRequired: Boolean);
begin
  Fname        := AName;
  FclassType   := AClassType;
  FisArray     := bIsArray;
  Fdescription := ADescription;
  Frequired    := bRequired;
end;

constructor SwagParamBody.create(AName: string; AClassType: TClass; ADescription: string);
begin
  create(AName, AClassType, False, ADescription, True);
end;

{ SwagResponse }

constructor SwagResponse.create(AHttpCode: Integer; ASchema, ADescription: String; bIsArray: Boolean);
begin
  FhttpCode    := AHttpCode;
  Fschema      := ASchema;
  Fdescription := ADescription;
  FisArray     := bIsArray;
end;

constructor SwagResponse.create(AHttpCode: Integer; AClassType: TClass; ADescription: String; bIsArray: Boolean);
begin
  FhttpCode    := AHttpCode;
  FclassType   := AClassType;
  Fdescription := ADescription;
  FisArray     := bIsArray;
end;

function SwagResponse.classType: TClass;
begin
  result := FclassType;
end;

constructor SwagResponse.create(AHttpCode: Integer);
begin
  FhttpCode:= AHttpCode;

end;

{ SwaggerApp }

constructor SwagApp.create(ATitle, AVersion, AHost: String);
begin
  FTitle    := ATitle;
  FVersion := AVersion;
  FHost     := AHost;
end;

constructor SwagApp.create(ATitle: String);
begin
  create(ATitle, EmptyStr, EmptyStr);
end;

{ SwagAppDescription }

constructor SwagAppDescription.create(ADescription: String);
begin
  FDescription := ADescription;
end;

{ SwagBasePath }

constructor SwagBasePath.create(Value: String);
begin
  FValue := Value;
end;

end.

