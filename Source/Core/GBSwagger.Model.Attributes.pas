unit GBSwagger.Model.Attributes;

interface

uses
  System.SysUtils;

type
  SwagClass = class(TCustomAttribute)
  private
    Fdescription: String;
    public
      constructor create(ADescription: String); overload;

      property description: String read Fdescription;
  end;

  SwagRequired = class(TCustomAttribute)
  end;

  SwagDescType = class(TCustomAttribute)
  private
    Fdescription: string;
  public
    property description: string read Fdescription;
    constructor Create(ADescription: string);
  end;

  SwagDataType = class(SwagDescType)
  private
    Fname: string;
    FreadOnly: Boolean;
  public
    property name: string read Fname;
    property readOnly: Boolean read FreadOnly write FreadOnly;
    constructor create(AName: string = ''; ADescription: string = ''; bReadOnly: Boolean = False);
  end;

  SwagString = class(SwagDataType)
  private
    FmaxLength: Integer;
    FminLength: Integer;
  public
    property minLength: Integer read FminLength;
    property maxLength: Integer read FmaxLength;

    constructor create(AMinLength, AMaxLength: Integer; ADescription: string = ''); overload;
    constructor create(AMaxLength: Integer; ADescription: string = ''); overload;

    constructor create(ADescription: string; AMinLength, AMaxLength: Integer); overload;
    constructor create(ADescription: string; AMaxLength: Integer); overload;
  end;

  SwagNumber = class(SwagDataType)
  private
    Fminimum: Double;
    Fmaximum: Double;

    public
      constructor create(AMinimum: Double;
                         AMaximum: Double = 0;
                         ADescription: String = ''); overload;

      property minimum: Double read Fminimum write Fminimum;
      property maximum: Double read Fmaximum write Fmaximum;
  end;

implementation

{ SwagClass }

constructor SwagClass.create(ADescription: String);
begin
  Fdescription:= ADescription;
end;

{ SwagNumber }

constructor SwagNumber.create(AMinimum: Double;
                         AMaximum: Double = 0;
                         ADescription: String = '');
begin
  Fminimum := AMinimum;
  Fmaximum := AMaximum;
  Fdescription := ADescription;
end;

{ SwagString }

constructor SwagString.create(AMaxLength: Integer; ADescription: string = '');
begin
  create(0, AMaxLength, ADescription);
end;

constructor SwagString.create(AMinLength, AMaxLength: Integer; ADescription: string = '');
begin
  FminLength   := AMinLength;
  FmaxLength   := AMaxLength;
  Fdescription := ADescription;
end;

constructor SwagString.create(ADescription: string; AMinLength, AMaxLength: Integer);
begin
  create(AMinLength, AMaxLength, ADescription);
end;

constructor SwagString.create(ADescription: string; AMaxLength: Integer);
begin
  create(0, AMaxLength, ADescription);
end;

{ SwagDataType }

constructor SwagDataType.create(AName, ADescription: string; bReadOnly: Boolean);
begin
  Fname        := AName;
  Fdescription := ADescription;
  FreadOnly    := bReadOnly;
end;

{ SwagDescType }

constructor SwagDescType.Create(ADescription: string);
begin
  Fdescription := ADescription;
end;


end.

