unit GBSwagger.Model.Types;

interface

uses
  System.SysUtils,
  Web.HTTPApp;

const
  SWAG_STRING  = 'string';
  SWAG_INTEGER = 'integer';

type
  TGBSwaggerContentType = (gbAppJSON, gbAppXML, gbTextHtml, gbPlainText, gbMultiPartFormData);
  TGBSwaggerProtocol    = (gbHttp, gbHttps);
  TGBSwaggerParamType   = (gbHeader, gbBody, gbQuery, gbPath, gbFormData);
  TGBSwaggerSecurityType = (gbBasic, gbApiKey, gbOAuth2);
  TGBSwaggerSecurityFlow = (gbImplicit, gbPassword, gbApplication, gbAccessCode);

  TGBSwaggerContentTypeHelper = record helper for TGBSwaggerContentType
    public
      function toString: string;
  end;

  TGBSwaggerProtocolHelper = record helper for TGBSwaggerProtocol
    public
      function toString: string;
  end;

  TGBSwaggerParamTypeHelper = record helper for TGBSwaggerParamType
    public
      function toString: string;
  end;

  TMethodTypeHelper = record helper for TMethodType
    public
      function toString: string;
  end;

  TGBSwaggerSecurityTypeHelper = record helper for TGBSwaggerSecurityType
    public
      function toString: string;
  end;

  TGBSwaggerSecurityFlowHelper = record helper for TGBSwaggerSecurityFlow
    public
      function toString: string;
  end;

implementation

{ TGBSwaggerProtocolHelper }

function TGBSwaggerProtocolHelper.toString: string;
begin
  case Self of
    gbHttp  : result := 'http';
    gbHttps : Result := 'https';
  end;
end;

{ TGBSwaggerContentTypeHelper }

function TGBSwaggerContentTypeHelper.toString: string;
begin
  case Self of
    gbAppJSON           : result := 'application/json';
    gbAppXML            : result := 'application/xml';
    gbTextHtml          : result := 'text/html';
    gbPlainText         : result := 'text/plain';
    gbMultiPartFormData : result := 'multipart/form-data';
  end;
end;

{ TMethodTypeHelper }

function TMethodTypeHelper.toString: string;
begin
  case Self of
    mtAny    : result := 'any';
    mtGet    : result := 'get';
    mtPut    : result := 'put';
    mtPost   : result := 'post';
    mtHead   : result := 'head';
    mtDelete : result := 'delete';
    mtPatch  : result := 'patch';
  end;
end;

{ TGBSwaggerParamTypeHelper }

function TGBSwaggerParamTypeHelper.toString: string;
begin
  case Self of
    gbHeader   : Result := 'header';
    gbBody     : Result := 'body';
    gbQuery    : Result := 'query';
    gbPath     : Result := 'path';
    gbFormData : Result := 'formData';
  end;
end;

{ TGBSwaggerSecurityTypeHelper }

function TGBSwaggerSecurityTypeHelper.toString: string;
begin
  case Self of
    gbBasic  : result := 'basic';
    gbApiKey : result := 'apiKey';
    gbOAuth2 : result := 'oauth2';
  end;
end;

{ TGBSwaggerSecurityFlowHelper }

function TGBSwaggerSecurityFlowHelper.toString: string;
begin
  case Self of
    gbImplicit    : result := 'implicit';
    gbPassword    : result := 'password';
    gbApplication : result := 'application';
    gbAccessCode  : result := 'accessCode';
  end;
end;

end.
