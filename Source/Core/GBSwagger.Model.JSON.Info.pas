unit GBSwagger.Model.JSON.Info;

interface

uses
  GBSwagger.Model.JSON.Interfaces,
  GBSwagger.Model.Interfaces,
  GBSwagger.Model.Types,
  GBSwagger.Model.JSON.Contact,
  System.JSON;

type TGBSwaggerModelJSONInfo = class(TInterfacedObject, IGBSwaggerModelJSON)

  private
    FSwaggerInfo: IGBSwaggerInfo;

  public
    constructor create(SwaggerInfo: IGBSwaggerInfo);
    class function New(SwaggerInfo: IGBSwaggerInfo): IGBSwaggerModelJSON;

    function ToJSON: TJSONValue;

end;

implementation

{ TGBSwaggerModelJSONInfo }

constructor TGBSwaggerModelJSONInfo.create(SwaggerInfo: IGBSwaggerInfo);
begin
  FSwaggerInfo := SwaggerInfo;
end;

class function TGBSwaggerModelJSONInfo.New(SwaggerInfo: IGBSwaggerInfo): IGBSwaggerModelJSON;
begin
  Result := Self.create(SwaggerInfo);
end;

function TGBSwaggerModelJSONInfo.ToJSON: TJSONValue;
begin
  result := TJSONObject.Create
              .AddPair('description', FSwaggerInfo.Description)
              .AddPair('version', FSwaggerInfo.Version)
              .AddPair('title', FSwaggerInfo.Title)
              .AddPair('termsOfService', FSwaggerInfo.TermsOfService)
              .AddPair('contact', TGBSwaggerModelJSONContact.New(FSwaggerInfo.Contact).ToJSON)
              .AddPair('license', TGBSwaggerModelJSONContact.New(FSwaggerInfo.License).ToJSON);
end;

end.
