unit Horse.GBSwagger;

interface

uses
  Horse,
  GBSwagger.Model.Interfaces,
  GBSwagger.Model.JSON.Interfaces,
  GBSwagger.Model.Types,
  GBSwagger.Web.HTML,
  System.JSON,
  Rest.JSON,
  System.StrUtils,
  System.SysUtils,
  Web.HTTPApp;

const
  SWAG_STRING  = GBSwagger.Model.Types.SWAG_STRING;
  SWAG_INTEGER = GBSwagger.Model.Types.SWAG_INTEGER;

  PATH_HTML = '/swagger/doc/html';
  PATH_JSON = '/swagger/doc/json';

type
  TGBSwaggerContentType = GBSwagger.Model.Types.TGBSwaggerContentType;
  TGBSwaggerProtocol    = GBSwagger.Model.Types.TGBSwaggerProtocol;
  TGBSwaggerParamType   = GBSwagger.Model.Types.TGBSwaggerParamType;

function HorseSwagger: THorseCallback; overload;
function HorseSwagger(APathHtml: String; APathJSON: string = ''): THorseCallback; overload;

procedure SwaggerHTML(ARequest: THorseRequest; AResponse: THorseResponse; ANext: TProc);
procedure SwaggerJSON(ARequest: THorseRequest; AResponse: THorseResponse; ANext: TProc);

procedure SwaggerInitialize(APathHtml: String; APathJSON: string = '');

var
  JSONSwagger: string;
  JSONPath   : string;
  HTMLSwagger: string;
  Swagger    : IGBSwagger;

implementation

function HorseSwagger(APathHtml: String; APathJSON: string = ''): THorseCallback;
begin
  JSONPath := IfThen(APathJSON.IsEmpty, PATH_JSON, APathJSON);
  JSONPath := IfThen(JSONPath.StartsWith('/'), JSONPath, '/' + JSONPath);

  SwaggerInitialize(APathHtml, JSONPath);

  THorse.GetInstance.Get(APathHtml, SwaggerHTML);

  if IsLibrary then
    THorse.GetInstance.Get(JSONPath, SwaggerJSON);

  result :=
    procedure(ARequest: THorseRequest; AResponse: THorseResponse; ANext: TProc)
    begin
      try
        ANext();
      finally
      end;
    end;
end;

function HorseSwagger: THorseCallback;
begin
  result := HorseSwagger(PATH_HTML, PATH_JSON);
end;

function HostRequest(ARequest: THorseRequest):string;
var
  LWebRequest : TWebRequest;
  LHost : string;
  LPort : Integer;
  LProtocol : string;
begin
  LWebRequest := THorseHackRequest(ARequest).GetWebRequest;
  LHost := LWebRequest.Host;
  LPort := LWebRequest.ServerPort;
  if ((LPort <> 80) and (LPort <> 443)) then
    LHost := LHost +':'+LPort.ToString;

  LProtocol := 'http://';
  if LWebRequest.ProtocolVersion.ToLower.StartsWith('https') then
    LProtocol := 'https://';

  Result := LProtocol + LHost;
end;

procedure SwaggerHTML(ARequest: THorseRequest; AResponse: THorseResponse; ANext: TProc);
var
  //lProtocol: String;
  lpathJSON: String;
  lBasePath: string;
  LHost : string;
begin
  LHost := HostRequest(ARequest);
  //if HTMLSwagger.IsEmpty then
  //begin
    lBasePath := Swagger.BasePath;
    lBasePath := IfThen(lBasePath.EndsWith('/'), Copy(lBasePath, 1, lBasePath.Length - 1), lBasePath);

    if not IsLibrary then
      THorse.GetInstance.Get(lBasePath + JSONPath, SwaggerJSON);

    // Compatibilidade com projetos dlls. Colaboração Paulo Monteiro
    //lProtocol := Swagger.Protocols[Pred(Length(Swagger.Protocols))].toString;
    lpathJSON := format('%s%s%s', [LHost, lBasePath, JSONPath]);
    HTMLSwagger := SwaggerDocument(Swagger.Config.ResourcePath, lpathJSON);
  //end;

  AResponse.Send(HTMLSwagger);
end;

procedure SwaggerJSON(ARequest: THorseRequest; AResponse: THorseResponse; ANext: TProc);
begin
  if JSONSwagger.IsEmpty then
    JSONSwagger := SwaggerJSONString(Swagger);

  AResponse.Send(JSONSwagger);
end;

procedure SwaggerInitialize(APathHtml: String; APathJSON: string = '');
//var
//  horse: THorse;
begin
  THorse.GetInstance;
//  horse := THorse.GetInstance as THorse;
//  Swagger
//    .Host('localhost');
//
//  if horse.Port > 0 then
//    Swagger.Host('localhost:' + horse.Port.ToString);
end;

initialization
  JSONSwagger := EmptyStr;
  HTMLSwagger := EmptyStr;
  Swagger     := GBSwagger.Model.Interfaces.Swagger;

end.
